﻿using System;
using System.Windows.Input;

namespace tolalexol.MVVM.Commands
{
	public class DelegateCommand : ICommand
	{
		private readonly Predicate<object> _canExecute;
		private readonly Action<object> _action;

		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

		public DelegateCommand(Action<object> action, Predicate<object> canExecute = null)
		{
			_action = action;
			_canExecute = canExecute;
		}

		public bool CanExecute(object parameter) =>
			_canExecute is null || _canExecute(parameter);

		public void Execute(object parameter) =>
			_action(parameter);
	}
}