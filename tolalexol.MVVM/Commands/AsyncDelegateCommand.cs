﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace tolalexol.MVVM.Commands
{
	public class AsyncDelegateCommand : ICommand
	{
		private readonly Predicate<object> _canExecute;
		private readonly Func<object, Task> _asyncAction;

		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

		public AsyncDelegateCommand(Func<object, Task> asyncAction, Predicate<object> canExecute = null)
		{
			_asyncAction = asyncAction;
			_canExecute = canExecute;
		}

		public bool CanExecute(object parameter) =>
			_canExecute is null || _canExecute(parameter);

		public async void Execute(object parameter) =>
			await ExecuteAsync(parameter);

		protected virtual async Task ExecuteAsync(object parameter) =>
			await _asyncAction(parameter);
	}
}