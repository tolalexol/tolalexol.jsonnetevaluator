﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Windows.Input;
using tolalexol.JsonNETEvaluator.Properties;
using tolalexol.MVVM;
using tolalexol.MVVM.Commands;

namespace tolalexol.JsonNETEvaluator.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		#region Fields

		private string _jsonPathSyntax;
		private string _json;

		#endregion

		#region Properties

		public string JSONPathSyntax
		{
			get => _jsonPathSyntax;
			set
			{
				_jsonPathSyntax = value;
				OnPropertyChanged(nameof(JSONPathSyntax));
				OnPropertyChanged(nameof(EvaluationResult));
			}
		}

		public string JSON
		{
			get => _json;
			set
			{
				_json = value;
				OnPropertyChanged(nameof(JSON));
				OnPropertyChanged(nameof(EvaluationResult));
			}
		}

		public string EvaluationResult
		{
			get
			{
				try
				{
					var jObject = JToken.Parse(JSON);
					var result = String.Join(",\r\n", jObject.SelectTokens(JSONPathSyntax));

					return !String.IsNullOrEmpty(result) ? result : "No match";
				}
				catch (Exception ex)
				{
					return ex.Message;
				}
			}
		}

		#endregion

		#region Commands

		public ICommand OpenWebPageCommand { get; }

		#endregion

		#region Constructors

		public MainWindowViewModel()
		{
			JSONPathSyntax = Strings.JSONPathSyntaxExample;
			JSON = Strings.JSONExample;

			OpenWebPageCommand = new DelegateCommand(a => OpenWebPage());
		}

		#endregion

		#region Methods

		private void OpenWebPage()
		{
			Process.Start(new ProcessStartInfo("https://goessner.net/articles/JsonPath/index.html#e2"));
		}

		#endregion
	}
}